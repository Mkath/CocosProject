package com.cerezaconsulting.cocosapp.presentation.main.home;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cerezaconsulting.cocosapp.R;
import com.cerezaconsulting.cocosapp.core.BaseFragment;

/**
 * Created by kath on 15/12/17.
 */

public class FragmentCercaDeMi extends BaseFragment {

    public FragmentCercaDeMi() {
    }

    public static FragmentCercaDeMi newInstance() {
        return new FragmentCercaDeMi();
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_proximamente, container, false);
        return  rootView;
    }


}
