package com.cerezaconsulting.cocosapp.presentation.main.favoritos;

import com.cerezaconsulting.cocosapp.core.BasePresenter;
import com.cerezaconsulting.cocosapp.core.BaseView;
import com.cerezaconsulting.cocosapp.data.entities.RestEntinty;

import java.util.ArrayList;

/**
 * Created by katherine on 12/05/17.
 */

public interface FavoritosContract {
    interface View extends BaseView<Presenter> {

        void getListRestaurante(ArrayList<RestEntinty> list);

        void clickItemRestaurante(RestEntinty restEntinty);

        boolean isActive();
    }

    interface Presenter extends BasePresenter {


        void loadOrdersFromPage(int page, int id);

        void loadfromNextPage(int id);

        void startLoad(int id);

        void getRestaurante(int page, int id);

    }
}
