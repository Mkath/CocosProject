package com.cerezaconsulting.cocosapp.presentation.main.promociones;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cerezaconsulting.cocosapp.R;
import com.cerezaconsulting.cocosapp.core.BaseFragment;
import com.cerezaconsulting.cocosapp.data.entities.DescEntity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by katherine on 28/06/17.
 */

public class PromoPropiaFragment extends BaseFragment {

    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.tv_precio)
    TextView tvPrecio;
    @BindView(R.id.image_container)
    RelativeLayout imageContainer;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.tv_text)
    TextView tvText;
    @BindView(R.id.tv_terms)
    TextView tvTerms;
    Unbinder unbinder;
    private DescEntity descEntity;

    public PromoPropiaFragment() {
        // Requires empty public constructor
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    public static PromoPropiaFragment newInstance(Bundle bundle) {
        PromoPropiaFragment fragment = new PromoPropiaFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        descEntity = (DescEntity) getArguments().getSerializable("descEntity");
        //restEntinty = (RestEntinty) getArguments().getSerializable("restEntity");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_discount, container, false);
        unbinder = ButterKnife.bind(this, root);
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (descEntity != null) {

            if (descEntity.getPhoto() != null) {
                Glide.with(getContext())
                        .load(descEntity.getPhoto())
                        .into(((image)));
            }

            tvPrecio.setText(String.valueOf(descEntity.getPorc()));
            tvText.setText(descEntity.getDescrip());
            tvTitle.setText(descEntity.getName());
        }


    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();

    }

    @OnClick(R.id.tv_terms)
    public void onViewClicked() {
        Bundle bundle = new Bundle();
        bundle.putString("msg", descEntity.getTerms_condition());
        DialogTerminos dialogTerminos = new DialogTerminos(getContext(),bundle);
        dialogTerminos.show();
    }
}
